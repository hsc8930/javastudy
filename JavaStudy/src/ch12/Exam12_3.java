/*회원가입을 하면서 아이디를 입력 받으려고 하는데 서비스의 정책 상 아이디에
 대문자와 소문자가 모두 사용되어야 한다.
 사용자로부터 입력받은 아이디를 검사하는 코드를 작성하시오.
 */

/* 반복문을 이용하여 문자열로부터 문자 꺼내오기 */

/* A ~ Z, a ~ z 사이의 문자이면 아스키 코드를 2 증가시킨 후 출력 */
/* 단, X / x / Z / z 의 경우 2 증가 시 */
/* 알파벳 범위를 벗어나지 않도록 처리 */
/* A ~ Z, a ~ z 사이의 문자가 아니면 그대로 출력 */
package ch12;

public class Exam12_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "동해물과 백두산이 마르고 닳도록 하느님이 보우하사 우리나라 만세";
		int idx = 0;
		while(true) {
			idx = str.indexOf(" ", idx+1);
			if(idx == -1) {
				System.out.println(str);
				break;
			}
			System.out.println(str.substring(0,idx));
		}
	}
}
