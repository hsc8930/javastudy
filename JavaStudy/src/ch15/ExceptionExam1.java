package ch15;

import java.io.File;
import java.io.IOException;

public class ExceptionExam1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String numStr = " 123";
		try {
			int num = Integer.parseInt(numStr); // NumberFormatException
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			//new File("").createNewFile();
		}

		try {
			Object obj = new String("a");
			int a = (Integer) obj; // ClassCastException
		} catch (ClassCastException e) {
			System.out.println("���ܹ߻�");
		}

	}

}
