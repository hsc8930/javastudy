package ch06;

public class Exam6_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 100;
		int total = gettotal(n);
		System.out.println("1 ~ " + n + " 까지의 합 : " + total);
	}

	// 메소드 작성
	public static int gettotal(int n) {
		int sum = 0;
		for (int i = 0; i <= n; i++) {
			sum += i;
		}
		return sum;
	}

}
