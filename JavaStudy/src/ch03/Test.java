package ch03;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		boolean result = 1 + 2 / 2 >= 1 + 1 * 2;
		System.out.println("결과 : " + result);

		float div = 5 / 2f;
		System.out.println("나누기 결과 : " + div);

		int a = 23;
		int b = 5;
		int div1 = a / b;
		int mod = a % b;/* 나머지 구하기 */
		System.out.println("몫 : " + div1);
		System.out.println("나머지 : " + mod);

		int num1 = 456;
		System.out.println((num1 / 100) * 100); // 나누기, 곱하기 연산자 이용

		int num2 = 333;
		System.out.println((num2 / 10) * 10 + 1); // 나누기, 곱하기 연산자 이용

		Scanner scanner = new Scanner(System.in);
		System.out.print("2자리수 정수 입력(10~99)>>");
		int n = scanner.nextInt();
		int ten = 0; // 10의 자리 수를 저장할 변수
		int one = 0; // 1의 자리 수를 저장할 변수
		boolean isMatch = false;
		// 두 자리 수 비교 값을 저장할 변수
		/* ten 변수에 10의 자리 수 대입 */ten = n / 10;
		/* one 변수에 1의 자리 수 대입 */one = n % 10;
		/* isMatch 변수에 두 개의 자리 수 비교 값 저장 */isMatch = ten == one;
		System.out.println(isMatch);
		scanner.close();

		int number = 12345;
		// 코드 작성 (% 연산자)
		int number5 = number % 10;
		number = number / 10;
		int number4 = number % 10;
		number = number / 10;
		int number3 = number % 10;
		number = number / 10;
		int number2 = number % 10;
		number = number / 10;
		int number1 = number % 10;
		int total = number1 + number2 + number3 + number4 + number5;
		System.out.println("각 자리 숫자의 합 : " + total);

		int num4 = 10;
		System.out.println(num4 < 0 ? "음수" : "양수"); // 삼항 연산자

		char ch = 'T';
		char lowerCase = (char) (( /* A 보다 크고 Z 보다 적은 경우 */ch > 65 && ch < 96) ? (ch + 32) : ch);
		System.out.println("ch : " + ch);
		System.out.println("ch to lowerCase : " + lowerCase);

	}
}
